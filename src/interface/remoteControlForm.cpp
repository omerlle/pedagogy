/*
 * remoteControlForm.cpp
 *
 *  Created on: 27 במאי 2020
 *      Author: omer levin <omerlle@gmail.com>
 */
//#include <iostream>

#include "remoteControlForm.h"
#include "timetableexport.h"
#include "lockunlock.h"
#include "solution.h"
#include "rules.h"
#include "fet.h"

#include <QXmlStreamReader>
#include <cassert>

//Represents the current status of the simulation - running or stopped.
extern bool simulation_running;

extern bool students_schedule_ready;
extern bool teachers_schedule_ready;
extern bool rooms_schedule_ready;

extern Solution best_solution;

extern Rules rules2;

extern QApplication* pqapplication;

RemoteControlForm::RemoteControlForm(QWidget* parent, WebSocket* webSocket): QDialog(parent), timetableGenerateForm(NULL),remote(webSocket),currentIndex(-1){
	setupUi(this);
	setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    connect(this, &RemoteControlForm::remoteControlReply, webSocket, &WebSocket::reply);
    connect(this, &RemoteControlForm::gotWarning, webSocket, &WebSocket::sendWarning);
	connect(webSocket, &WebSocket::endRemoteControl, this, &RemoteControlForm::closeForm);
    connect(webSocket, &WebSocket::importXmlRules, this, &RemoteControlForm::updateRules);
    connect(webSocket, &WebSocket::timetableGenerate, this, &RemoteControlForm::timetableGenerate);
    connect(webSocket, &WebSocket::getResults, this, &RemoteControlForm::sendResults);
    connect(webSocket, &WebSocket::getActivities, this, &RemoteControlForm::sendActivities);
    statusTextEdit->setPlainText(tr("ready"));
    setObjectName("do not show messages");
}

RemoteControlForm::~RemoteControlForm() {}
void RemoteControlForm::closeForm(int index)
{
	this->close();
	sendReply(index, true, "");
}
void RemoteControlForm::updateRules(int index)
{
	if(simulation_running){
		sendReply(index, false, tr("Allocation in course.\nPlease stop simulation before this."));
		return;
	}
	//notify the user modifier deleted
	QString modifiedString(gt.rules.modified ? tr("(modifier deleted)") : "");

	//QCursor orig=this->cursor();
	//this->setCursor(Qt::WaitCursor);

	statusTextEdit->setPlainText(tr("Loading rules...%1").arg(modifiedString));
	pqapplication->processEvents();

		//bool before=gt.rules.modified;
	gt.rules.modified=true; //to avoid flicker of the main form modified flag
	QByteArray xml;
	remote->getXmlForImport(xml);
	QXmlStreamReader xmlReader(xml);
	if(gt.rules.update(this, xmlReader)){
		teachers_schedule_ready=false;
		students_schedule_ready=false;
		rooms_schedule_ready=false;
		LockUnlock::computeLockedUnlockedActivitiesTimeSpace();
		LockUnlock::increaseCommunicationSpinBox();
		QString result(tr("import complete%1").arg(modifiedString));
		statusTextEdit->setPlainText(result);
		gt.rules.modified=true; //force update of the modified flag of the main window
		setRulesUnmodifiedAndOtherThings(&gt.rules);
		INPUT_FILENAME_XML="";
		parentWidget()->setWindowTitle(tr("%1[*] - PEDAGOGY", "The title of the main window, %1 is the name of the current file. "
			 "Please keep the string [*] unmodified (three characters) - it is used to make the difference between modified files and unmodified files.")
			 .arg("portal"));
		sendReply(index, true, result);
	}
	else{
		//incorrect code - the old file may be broken - so we generate a new file.
		/*gt.rules.modified=before;

		statusBar()->showMessage("", STATUS_BAR_MILLISECONDS);

		setCurrentFile(INPUT_FILENAME_XML);*/

		assert(!simulation_running);
		gt.rules.modified=false;
		qobject_cast<FetMainForm*>(parentWidget())->on_fileNewAction_triggered();
		sendReply(index, false, tr("import fail%1").arg(modifiedString));
	}
}
void RemoteControlForm::timetableGenerate(int index){
	assert(timetableGenerateForm == 0);
	currentIndex=index;
	timetableGenerateForm = new TimetableGenerateForm(this);
	setParentAndOtherThings(timetableGenerateForm, this);
	connect(timetableGenerateForm, &TimetableGenerateForm::gotActivityPlaced,qobject_cast<WebSocket*>(sender()) , &WebSocket::sendStatus);
	connect(timetableGenerateForm, &TimetableGenerateForm::generateFinish,this , &RemoteControlForm::generateFinish);
	connect(qobject_cast<WebSocket*>(sender()) , &WebSocket::stopGenerate, timetableGenerateForm, &TimetableGenerateForm::stop);
	timetableGenerateForm->start();
}
void RemoteControlForm::generateFinish()
{
	currentIndex=-1;
	timetableGenerateForm->deleteLater();
	timetableGenerateForm=NULL;
	emit remoteControlReply();
}
void RemoteControlForm::sendResults(int index){
	if(!students_schedule_ready || !teachers_schedule_ready || !rooms_schedule_ready){
		sendReply(index, false, tr("You have not yet generated a timetable - please generate firstly"));
		return;
	}

	Solution* tc=&best_solution;

	for(int ai=0; ai<gt.rules.nInternalActivities; ai++){
		//Activity* act=&gt.rules.internalActivitiesList[ai];
		int time=tc->times[ai];
		if(time==UNALLOCATED_TIME){
			sendReply(index, false, tr("It seems that you have an incomplete timetable."
			 " Saving of timetable does not work for incomplete timetables. Please generate a complete timetable"));
			 //.arg(act->id));
			return;
		}

		int ri=tc->rooms[ai];
		if(ri==UNALLOCATED_SPACE){
			sendReply(index, false, tr("It seems that you have an incomplete timetable."
			 " Saving of timetable does not work for incomplete timetables. Please generate a complete timetable"));
			 //.arg(act->id));
			return;
		}
	}

	rules2.initialized=true;

	rules2.institutionName=gt.rules.institutionName;
	rules2.comments=gt.rules.comments;

	rules2.nHoursPerDay=gt.rules.nHoursPerDay;
	for(int i=0; i<gt.rules.nHoursPerDay; i++)
		rules2.hoursOfTheDay[i]=gt.rules.hoursOfTheDay[i];

	rules2.nDaysPerWeek=gt.rules.nDaysPerWeek;
	for(int i=0; i<gt.rules.nDaysPerWeek; i++)
		rules2.daysOfTheWeek[i]=gt.rules.daysOfTheWeek[i];

	rules2.yearsList=gt.rules.yearsList;

	rules2.teachersList=gt.rules.teachersList;

	rules2.subjectsList=gt.rules.subjectsList;

	rules2.activityTagsList=gt.rules.activityTagsList;

	rules2.activitiesList=gt.rules.activitiesList;

	rules2.buildingsList=gt.rules.buildingsList;

	rules2.roomsList=gt.rules.roomsList;

	rules2.timeConstraintsList=gt.rules.timeConstraintsList;

	rules2.spaceConstraintsList=gt.rules.spaceConstraintsList;

	rules2.apstHash=gt.rules.apstHash;
	rules2.aprHash=gt.rules.aprHash;

	rules2.groupActivitiesInInitialOrderList=gt.rules.groupActivitiesInInitialOrderList;

	//add locking constraints
	TimeConstraintsList lockTimeConstraintsList;
	SpaceConstraintsList lockSpaceConstraintsList;

	//bool report=true;

	int addedTime=0, duplicatesTime=0;
	int addedSpace=0, duplicatesSpace=0;

	QString constraintsString=QString("");

	//lock selected activities
	for(int ai=0; ai<gt.rules.nInternalActivities; ai++){
		Activity* act=&gt.rules.internalActivitiesList[ai];
		int time=tc->times[ai];
		if(time>=0 && time<gt.rules.nDaysPerWeek*gt.rules.nHoursPerDay){
			int hour=time/gt.rules.nDaysPerWeek;
			int day=time%gt.rules.nDaysPerWeek;

			ConstraintActivityPreferredStartingTime* ctr=new ConstraintActivityPreferredStartingTime(100.0, act->id, day, hour, false); //permanently locked is false
			bool t=rules2.addTimeConstraint(ctr);

			if(t){
				addedTime++;
				lockTimeConstraintsList.append(ctr);
			}
			else
				duplicatesTime++;

			QString s;

			if(t)
				s=tr("Added to the saved file:", "It refers to a constraint")+QString("\n")+ctr->getDetailedDescription(gt.rules);
			else{
				s=tr("NOT added to the saved file (already existing):", "It refers to a constraint")+QString("\n")+ctr->getDetailedDescription(gt.rules);
				delete ctr;
			}

			constraintsString+=QString("\n");
			constraintsString+=s;
		}

		int ri=tc->rooms[ai];
		if(ri!=UNALLOCATED_SPACE && ri!=UNSPECIFIED_ROOM && ri>=0 && ri<gt.rules.nInternalRooms){
			QStringList tl;
			if(gt.rules.internalRoomsList[ri]->isVirtual==false)
				assert(tc->realRoomsList[ai].isEmpty());
			else
				for(int rr : qAsConst(tc->realRoomsList[ai]))
					tl.append(gt.rules.internalRoomsList[rr]->name);

			ConstraintActivityPreferredRoom* ctr=new ConstraintActivityPreferredRoom(100, act->id, (gt.rules.internalRoomsList[ri])->name, tl, false); //false means not permanently locked
			bool t=rules2.addSpaceConstraint(ctr);

			QString s;

			if(t){
				addedSpace++;
				lockSpaceConstraintsList.append(ctr);
			}
			else
				duplicatesSpace++;

			if(t)
				s=tr("Added to the saved file:", "It refers to a constraint")+QString("\n")+ctr->getDetailedDescription(gt.rules);
			else{
				s=tr("NOT added to the saved file (already existing):", "It refers to a constraint")+QString("\n")+ctr->getDetailedDescription(gt.rules);
				delete ctr;
			}

			constraintsString+=QString("\n");
			constraintsString+=s;
		}
	}

//TODO	LongTextMessageBox::largeInformation(parent, tr("PEDAGOGY information"), tr("Added %1 locking time constraints and %2 locking space constraints to saved file,"
//	" ignored %3 activities which were already fixed in time and %4 activities which were already fixed in space.").arg(addedTime).arg(addedSpace).arg(duplicatesTime).arg(duplicatesSpace)
//	+QString("\n\n")+tr("Detailed information about each locking constraint which was added or not (if already existing) to the saved file:")+QString("\n")+constraintsString
//	+QString("\n")+tr("Your current data file remained untouched (no locking constraints were added), so you can save it also, or generate different timetables."));
	QByteArray xml;
	QTextStream tos(&xml,QIODevice::Append);
	rules2.write(tos);
	tos.flush();
	for(TimeConstraint* tc : qAsConst(lockTimeConstraintsList))
		delete tc;
	lockTimeConstraintsList.clear();
	for(SpaceConstraint* sc : qAsConst(lockSpaceConstraintsList))
		delete sc;
	lockSpaceConstraintsList.clear();
	//while(!lockTimeConstraintsList.isEmpty())
	//	delete lockTimeConstraintsList.takeFirst();
	//while(!lockSpaceConstraintsList.isEmpty())
	//	delete lockSpaceConstraintsList.takeFirst();

	rules2.nHoursPerDay=0;
	rules2.nDaysPerWeek=0;

	rules2.yearsList.clear();

	rules2.teachersList.clear();

	rules2.subjectsList.clear();

	rules2.activityTagsList.clear();

	rules2.activitiesList.clear();

	rules2.buildingsList.clear();

	rules2.roomsList.clear();

	rules2.timeConstraintsList.clear();

	rules2.spaceConstraintsList.clear();

	rules2.apstHash.clear();
	rules2.aprHash.clear();

	rules2.groupActivitiesInInitialOrderList.clear();
	remote->setReplyDataPayload(xml);
	sendReply(index, true, "");
}
void RemoteControlForm::sendActivities(int index)
{
	if(simulation_running){
		sendReply(index, false, tr("Allocation in course.\nPlease stop simulation before this."));
		return;
	}

	if(!students_schedule_ready || !teachers_schedule_ready || !rooms_schedule_ready){
		sendReply(index, false, tr("You have not yet generated a timetable - please generate firstly"));
		return;
	}

	assert(gt.rules.initialized && gt.rules.internalStructureComputed);

	Solution* tc=&best_solution;

	for(int ai=0; ai<gt.rules.nInternalActivities; ai++){
		//Activity* act=&gt.rules.internalActivitiesList[ai];
		int time=tc->times[ai];
		if(time==UNALLOCATED_TIME){
			sendReply(index, false, tr("It seems that you have an incomplete timetable."
			 " Saving of timetable does not work for incomplete timetables. Please generate a complete timetable"));
			 //.arg(act->id));
			return;
		}

		int ri=tc->rooms[ai];
		if(ri==UNALLOCATED_SPACE){
			sendReply(index, false, tr("It seems that you have an incomplete timetable."
			 " Saving of timetable does not work for incomplete timetables. Please generate a complete timetable"));
			 //.arg(act->id));
			return;
		}
	}

//	computeHashForIDsTimetable();
//	computeActivitiesAtTime();
//	computeActivitiesWithSameStartingTime();

	QByteArray xml;
	QTextStream tos(&xml,QIODevice::Append);
	TimetableExport::writeActivitiesTimetableXml(tos);
	tos.flush();

	remote->setReplyDataPayload(xml);
	sendReply(index, true, "");
}
