/*
 * remoteControlForm.h
 *
 *  Created on: 27 במאי 2020
 *      Author: omer levin <omerlle@gmail.com>
 */

#ifndef SRC_INTERFACE_REMOTECONTROLFORM_H_
#define SRC_INTERFACE_REMOTECONTROLFORM_H_

#include "ui_remotecontrolform_template.h"

#include "timetablegenerateform.h"
#include "fetmainform.h"
#include "websocket.h"

#include <QDialog>

class RemoteControlForm : public QDialog, Ui::RemoteControlForm_template{
	Q_OBJECT
public:
	RemoteControlForm(QWidget* parent, WebSocket* webSocket);
	virtual ~RemoteControlForm();
	inline WebSocket * getRemote(){return remote;};
	inline int getIndex(){return currentIndex;};
	inline void sendReply(int index, bool success, const QString& message)
	{
		remote->setReplyData(index, success, message);
			emit remoteControlReply();
	}
public slots:
	void closeForm(int index);
	void updateRules(int index);
	void timetableGenerate(int index);
	void sendResults(int index);
	void sendActivities(int index);
private slots:
	void generateFinish();
signals:
	void remoteControlReply();
	void gotWarning(QByteArray * warning);
private:
	TimetableGenerateForm * timetableGenerateForm;
	WebSocket * remote;
	int currentIndex;
};

#endif /* SRC_INTERFACE_REMOTECONTROLFORM_H_ */
