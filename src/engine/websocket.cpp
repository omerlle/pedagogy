/*
 * websocket.cpp
 *
 *  Created on: 25 במאי 2020
 *      Author: omer
 *      Copyright (C) 2020 omer levin <omerlle@gmail.com>
 */

#include "websocket.h"

//#include <iostream>

#include <cassert>
#include <QRegularExpression>
#include <QMessageBox>
#include <QDataStream>
#include <QFile>
#include <QSettings>
#include <QCryptographicHash>
#include <bitset>

WebSocket::WebSocket(QWidget* gui):server(NULL),socket(NULL), isWabSocketInit(false), correntIndex(0), currentCommand(CMD_IDLE), suppressResults(false), xmlForImport(NULL)
{
	replyData.reset();
	frameData.reset();
	server = new QTcpServer(this);
	connect(server, SIGNAL(newConnection()), SLOT(newConnection()));
	server->setMaxPendingConnections(2);//the second connection use for allow send "500 Internal Server" error to the second connection.
	bool ok(false);
	uint port(QSettings(COMPANY, PROGRAM).value("WebSocket/port","12000").toUInt(&ok));
	ok&=(port<65536);
	if(ok)ok&=server->listen(QHostAddress::LocalHost, port);
	if(!ok){
		QMessageBox::critical(gui, tr("remote control disable"), tr("error when setting up the server(%1)").arg(server->errorString()));
		closeServer();
	}
}
void WebSocket::closeServer(){
	if(server){
		server->close();
		delete server;
		server=NULL;
	}
}
WebSocket::~WebSocket()
{
	disconnect();
	closeServer();
}
QString WebSocket::getCurrentOpcodeString(){
	Commands Command=(currentCommand==CMD_START)?pendingCommands.head():currentCommand;
	switch (Command){
		case CMD_IMPORT:return QString("export");
		case CMD_GENERATE:return QString("create");
		case CMD_EXPORT:return QString("import");
		case CMD_GET_ACTIVITIES:return QString("activity");
		case CMD_END:break;//return QString("unknown");
		case CMD_IDLE:
		case CMD_START:
		default:
			assert(0);
	}
	return QString("unknown");
}
void WebSocket::sendCommand(Commands Command)
{
	assert(currentCommand==CMD_IDLE);
	currentCommand=Command;
	correntIndex=(correntIndex+1)%1024;//for verify that we get the same command id we send
	switch(Command){
	case CMD_START:
		emit startRemoteControl(correntIndex);
		break;
	case CMD_END:
		emit endRemoteControl(correntIndex);
		break;
	case CMD_IMPORT:
		emit importXmlRules(correntIndex);
		break;
	case CMD_GENERATE:
		emit timetableGenerate(correntIndex);
	break;
	case CMD_EXPORT:
		emit getResults(correntIndex);
	break;
	case CMD_GET_ACTIVITIES:
		emit getActivities(correntIndex);
	break;
	default:assert(0);
	}
}
void WebSocket::setReplyData(int index, bool success, const QString &message)
{
	assert(currentCommand!=CMD_IDLE);	
	replyData.index=index;
	replyData.success=success;
	replyData.message=message;
}
//for avoid copy of big data e.g big xml.
void WebSocket::setReplyDataPayload(QByteArray & payload)
	{
		assert(replyData.payload==QByteArray());
		replyData.payload=std::move(payload);
		payload.clear();
	}
//for avoid copy of big xml.
void WebSocket::getXmlForImport(QByteArray & xml)
{
	xml=std::move(xmlForImport);
	xmlForImport.clear();
}
void WebSocket::encodeResponse(QByteArray &response,int opcode)
{
	assert(opcode<16);
	//we add the header in reverse order.
//			std::cout <<"encodeResponse:response:"<<response.toStdString()<<std::endl;
//			std::cout <<"response size:"<<response.size()<<std::endl;
			size_t size;

#if 0
			size_t payloadLen=response.size();
			size=response.size();
			if (payloadLen>125){
				int size_length;
				if(payloadLen<65536){
					size_length=16;
					payloadLen=126;
				}else{
					size_length=64;
					payloadLen=127;
				}
				//add extended payload length in reverse order.
				for(int i = 0; i < size_length; i+=8){
					response.push_front((char)(((size)&(0xFF << i)) >>i));
				}
//				std::cout <<"extended payload length:"<<std::endl;
//				for(int i = 0; i < size_length/8; i++){std::cout <<std::bitset<8>(response.at(i))<<",";}
//				std::cout <<std::endl<<"response size in binary:"<<std::bitset<64>(size)<<std::endl;
			}
			//add the second byte of the header.
			response.push_front(payloadLen);
			//add the first byte of the header.
			response.push_front(0x80 | opcode);
//			std::cout <<"first and second byte of the header:"<<+response.at(0)<<","<<+response.at(1)<<" size of frame:"<< response.size()<< std::endl;
			assert(socket->write(response)==response.size());
#else //support in sending more than one frame when response.size()>65535 some browsers dont get big frames.
			int first_message=1;
//			std::cout <<"response.size():"<< response.size() <<"\n";
			while(response.size()>0){
				QByteArray header;
				size=response.size()<65536?response.size():65535;
				if (size>125){
					header.push_front((char)(size&0xFF));
					header.push_front((char)((size&0xFF00)>>8));
//					std::cout <<"extended payload length:"<<std::bitset<8>(header.at(0)) <<","<<std::bitset<8>(header.at(1))<<std::endl;
//					std::cout <<"size:"<<std::bitset<16>(size)<<std::endl;
					header.push_front(126);
				}else header.push_front(size);

				int firstByte=first_message|(response.size()<65536?0x80:0);
//				std::cout "size:"<< size << " firstByte:" << firstByte <<"\n";
				header.push_front(firstByte);
				first_message=0;
			//for (int i = 0; i < response.size(); i++){
			//	std::cout << +response.at(i) << "-" << response.at(i)<< "+";
			//}
//				std::cout << std::endl;
				socket->write(header);
				socket->write(response.left(size));
				response.remove(0,size);
			}
#endif
			//on windows when refresh the browser the waitForBytesWritten() failed.
			//from qt documentation:Note: This function may fail randomly on Windows. Consider using the event loop and the bytesWritten() signal if your software will run on Windows.
			//we don't care if waitForBytesWritten() failed
//			assert(socket->waitForBytesWritten());
			socket->waitForBytesWritten();
}
void WebSocket::reply()
{
	assert(currentCommand!=CMD_IDLE);
	assert((currentCommand==CMD_END && pendingCommands.empty()) || (currentCommand!=CMD_END && !pendingCommands.empty()));
	assert(replyData.index==correntIndex);
//	check if need to stop the transaction.
	QString currentOpcodeString(getCurrentOpcodeString());
	if (!replyData.success || (suppressResults && !pendingCommands.empty())){
		//on import data stop transaction, we need to clear the xml data.
		if (pendingCommands.contains(CMD_IMPORT))xmlForImport.clear();
		//stop the transaction
		pendingCommands.clear();
		// if currentCommand is CMD_START, we don't send end because the remote control state not started.
		if (currentCommand!=CMD_START)pendingCommands.enqueue(CMD_END);
	}
	QByteArray response;
	Commands next_command=pendingCommands.empty()?CMD_IDLE:pendingCommands.dequeue();
	//check if we need to send response(when results not suppress and one of the following:failure, finish the transaction[before end], has payload, has message).
	if(!suppressResults && (!replyData.success || next_command==CMD_END || replyData.payload != QByteArray() || replyData.message != QString(""))){
		response.append(currentOpcodeString+" ");
		if (!replyData.success) response.append("failure");
		//if we finish the transaction on success.
		else if (next_command==CMD_END) response.append("success");
		else response.append("status");
		response.append(" ");
		response.append(replyData.message);
		//if there is big data to send we add it
		if (replyData.payload != QByteArray())
		{
			replyData.payload.push_front(response);
			response=std::move(replyData.payload);
		}
	}
	replyData.reset();

	currentCommand=CMD_IDLE;
	//checking if we finish the transaction
	if (next_command==CMD_IDLE)suppressResults=false;//when transection finish reset the suppressReseults flag
	//if transection not finish, continue to the next command
	else sendCommand(next_command);//set currentCommand
	
	//checking if we have response to send
	if (response!=QByteArray()){
		//encode the response and send it to the websocket..
		encodeResponse(response);
	}
}
void WebSocket::sendStatus(QByteArray * status)
{
		status->push_front("status ");
		sendData(status);//delete status
}
void WebSocket::sendWarning(QByteArray * warning)
{
		warning->push_front("warning ");
		sendData(warning);//delete warning
}
//sending data without to end the current command
void WebSocket::sendData(QByteArray * data)
{
	assert(currentCommand!=CMD_IDLE);
	data->push_front((getCurrentOpcodeString()+" ").toUtf8());
	if (!suppressResults){
		encodeResponse(*data);
	}
	delete data;
}
void WebSocket::newConnection()
{
	//check we have real connection
	if (server->hasPendingConnections()){
		QTcpSocket *s =server->nextPendingConnection();
		assert(s);
		if (!s)return;
		if(socket){
			if(currentCommand==CMD_IDLE){
				disconnect();
				assert(!socket);
			}else{//if we already have connection and we are busy, we send '"HTTP/1.1 500 Internal Server Error' to the second connection and close it
				static QByteArray already_have_connection("HTTP/1.1 500 Internal Server Error\r\n\r\n");
				int ret=s->write(already_have_connection);
				bool ans=s->waitForBytesWritten(3);
				assert(ans && (ret==already_have_connection.size()));
				s->close();
				s->deleteLater();
				return;
			}
		}
		//create new connection
		socket =s;
		assert(buffer==QByteArray() && !isWabSocketInit);
		connect(socket, SIGNAL(readyRead()), SLOT(handleRequest()));
		connect(socket, SIGNAL(disconnected()), SLOT(disconnect()));
	}
}
void WebSocket::disconnect()
{
	if(!socket) return;
	socket->deleteLater();
	socket->close();
	socket=NULL;
	buffer.clear();
	isWabSocketInit=false;
	if(currentCommand!=CMD_IDLE) suppressResults=true;
}
void WebSocket::handleRequest()
{
	bool can_process(true);
	assert(socket == static_cast<QTcpSocket*>(sender()) && buffer.size()==0);
    //read from the socket
	while (socket->bytesAvailable() > 0){
        buffer.append(socket->readAll());
    }
    while (can_process){
    	//we check if already done handshake
    	if (isWabSocketInit){
//    		std::cout <<"buffer.size:"<< buffer.size()<< "\n";
    		//decode the client request
    		//check if we already did the first step.
    		if(frameData.headerSize==0){
    			//check if we have enough bytes for the first step.
    			if(buffer.size()<2)can_process=false;
    			else{
    				//first step:find the headerSize, mask bit.
    				frameData.isMask=std::bitset<8>(buffer.at(1)).test(7);
    				frameData.headerSize=frameData.isMask?4:0;
    				switch(buffer.at(1) & 127){
    					case 127:frameData.headerSize+=10;
    						break;
    					case 126:frameData.headerSize+=4;
    						break;
    					default:frameData.headerSize+=2;
    				}

    			}
    		//check if we already did the second step.(if frameData.rsv==-1 the rsv uninitialize)
    		}else if (frameData.rsv==-1){
    			//check if we have enough bytes for the second step.
    			if(buffer.size()<frameData.headerSize)can_process=false;
    			else{
    				//second step:finish to decode the header(final frame(fin), mask bit, Opcode[, RSV1, RSV2, RSV3], data size, mask pattern) and delete it.
    				std::bitset<8> firstByte(buffer.at(0));
    				frameData.fin=firstByte.test(7);
    				frameData.rsv=static_cast<int>((std::bitset<8>{ 0b01110000 } & firstByte).to_ulong());//RSV1, RSV2, RSV3
    				int opcode=static_cast<int>((std::bitset<8>{ 0b00001111 } & firstByte).to_ulong());
    				assert(frameData.opcode!=-1||frameData.data==QByteArray());
    				frameData.bad_opcode=frameData.opcode!=-1 && opcode !=0;//if we don't at the first frame in the set and opcode !=0. raise error
    				if (frameData.opcode==-1 || opcode !=0)//we save the old opcode only if frameData.opcode!=-1 || opcode ==0 this mean we in valid frame set.
    					frameData.opcode=opcode;
//    				std::cout <<"frame opcode:"<< frameData.opcode << std::endl;
    				//set the payload length and the extended payload length if needed
    				frameData.size=buffer.at(1) & 127;
    				if (frameData.size>125){
    					if (frameData.size==126){
    		    			quint16 s;
    		    			QDataStream(buffer.mid(2,2)) >> s;
    		    			frameData.size=s;
    		    			frameData.index=4;
    					}else{//size==127
    						quint64 s;
    						QDataStream(buffer.mid(2,8)) >> s;
    						frameData.size=s;
    						frameData.index=10;
    					}
    				}else frameData.index=2;
//    				std::cout <<"frameData.size:"<< frameData.size << std::endl;
    				//set the mask
    				if(frameData.isMask){
    					frameData.mask=buffer.mid(frameData.index,4);
    					frameData.index+=4;
    				}
//    				for(int i = 0; i < frameData.index; i++){std::cout <<std::bitset<8>(buffer.at(i))<<std::endl;}
    				//delete the header.
    				buffer.remove(0, frameData.index);
    				frameData.index=0;
    			}
    		}else{
    			//do decode while we have data in the buffer
    			while(buffer.size()>0 && frameData.index<frameData.size){
    				//third step:decode frame data.
    				frameData.data.append(buffer.at(0)^frameData.mask.at((frameData.index++)%4));
//    				std::cout <<std::bitset<8>(buffer.at(0))<<std::endl;
    				buffer.remove(0,1);
    			}
//    			std::cout <<"frameData.data:\""<< frameData.data.toStdString() <<"\""<< std::endl;
    			//check if we finish to read frame data.
    			if (frameData.index<frameData.size)can_process=false;
    			else if(frameData.rsv!=0 || !frameData.isMask||(frameData.opcode != 1&&frameData.opcode !=8 && frameData.bad_opcode)){
//    				std::cout <<"frameData.data:\""<< frameData.data.toStdString() <<"\""<< std::endl;
    				QByteArray response("unknown failure ");
    				//check the header is valid
    				if(frameData.rsv!=0)response.append(tr("unsupported in rsv"));
    				else if(!frameData.isMask)response.append(tr("frame have to be masked"));
    				//frameData.opcode !=1 && frameData.opcode !=8 or we in set of frame and frameData.opcode !=0
    				else response.append(tr("unsupported this websocket opcode(%1)").arg(frameData.opcode));
    				encodeResponse(response);
    				frameData.reset();
    			//got closing handshake
    			}else if(frameData.opcode ==8){
    				//return close response
    				QByteArray response(QByteArray(""));
    				encodeResponse(response,8);
    				frameData.reset();
    			}else{//valid header
    				if (frameData.fin){
    					//separate the opcode and payload.
    					int payload_index=frameData.data.indexOf(" ");
    					QString opcode;
    					if(payload_index==-1){
    						opcode=frameData.data;
    					}else{
    						opcode=frameData.data.mid(0,payload_index);
    						frameData.data.remove(0,payload_index+1);
    					}
    					if(currentCommand!=CMD_IDLE &&  opcode!="stop"){
    						QByteArray response((opcode + " failure "+tr("ignoring from the new request, still handling in the old one.")).toUtf8());
    						encodeResponse(response);
    					}else if(opcode=="stop"){
    						if (currentCommand!=CMD_GENERATE){
    							QByteArray response(("stop failure "+tr("simulation not running")).toUtf8());
    							encodeResponse(response);
    						}else emit stopGenerate();
    					}else if (opcode=="export" || opcode=="create" || opcode=="import" || opcode=="activity" || opcode=="generate"){
    						if (opcode=="export" || opcode=="generate"){
    							assert(xmlForImport==QByteArray());
    							xmlForImport=std::move(frameData.data);
    						}
    						if (opcode=="export")pendingCommands.enqueue(CMD_IMPORT);
    						else if(opcode=="create")pendingCommands.enqueue(CMD_GENERATE);
        					else if(opcode=="import")pendingCommands.enqueue(CMD_EXPORT);
        					else if(opcode=="activity")pendingCommands.enqueue(CMD_GET_ACTIVITIES);
        					else if(opcode=="generate"){
        					    pendingCommands.enqueue(CMD_IMPORT);
        					    pendingCommands.enqueue(CMD_GENERATE);
        					    pendingCommands.enqueue(CMD_EXPORT);
        					}else assert(0);
    						pendingCommands.enqueue(CMD_END);
    						sendCommand(CMD_START);
    					}else{
    						QByteArray response((opcode +" failure "+tr("unknown opcode command")).toUtf8());
    						encodeResponse(response);
    					}
    					frameData.reset();
    				}else{//didn't finish to get the data
    					frameData.prepareForNextFrameInSet();
    				}
    			}
    		}
    	}else{//no handshake yet
//    		std::cout <<get http:<< buffer.toStdString() << std::endl;
    		int index(buffer.indexOf("\r\n\r\n"));
    		//check if we get all the http headers.
    		if(index!=-1){
    			assert(currentCommand==CMD_IDLE);
    			// checking the header is understood and has an correct value
    			//move the http_request to new QByteArray.
    			QByteArray http_request(buffer.left(index+2));
    			buffer.remove(0, index+4);
    			static QCryptographicHash sha1(QCryptographicHash::Sha1);
    			static QRegularExpression get_http_ver("^GET / HTTP/(\\d+.\\d+)");
    			static QByteArray ack_prefix("HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: ");
    			static QString origin=QSettings(COMPANY, PROGRAM).value("WebSocket/origin","*").toString();
    			if (origin != "*")origin="\r\nOrigin: "+origin +"\r\n";
    			QRegularExpressionMatch http_ver = get_http_ver.match(http_request.mid(0, http_request.indexOf("\r\n")).data());
    			QString prefix="\r\nSec-WebSocket-Key: ";
    			index=http_request.indexOf(prefix)+prefix.length();
    			//checking that this get http request of ver is 1.1, request upgrade to websocket and has field Sec-WebSocket-Key.
    			if (!http_ver.hasMatch() || http_ver.captured(1).toDouble()<1.1 || http_request.indexOf("\r\nUpgrade: websocket\r\n") == -1 || index==-1){
    				socket->write("HTTP/1.1 400 Bad Request\r\n\r\n");
    				socket->waitForBytesWritten();
    				disconnect();
    			//checking that the origin is correct.
    			}else if (origin != "*" && http_request.indexOf(origin) == -1){
    				socket->write("HTTP/1.1 403 Forbidden\r\n\r\n");
    				socket->waitForBytesWritten();
					disconnect();
				//verify .Sec-WebSocket-Version supported
    			}else if (http_request.indexOf("\r\nSec-WebSocket-Version: 13\r\n") == -1){
    				socket->write("HTTP/1.1\r\nSec-WebSocket-Version: 13\r\n\r\n");
    			}else{
				//generate Sec-WebSocket-Accept from Sec-WebSocket-Key
    				sha1.addData(http_request.mid(index,http_request.indexOf("\r\n",index)-index).append("258EAFA5-E914-47DA-95CA-C5AB0DC85B11"));
    			//send response of 101 Switching Protocols
    				socket->write(ack_prefix+sha1.result().toBase64()+"\r\n\r\n");
    				sha1.reset();
    				isWabSocketInit=socket->waitForBytesWritten();
    			//handshake done
    			}
    		}else if (buffer.size() > 1024){//too big http request
    			socket->write("HTTP/1.1 400 Bad Request\r\n\r\n");
    			socket->waitForBytesWritten();
    			disconnect();
    		}else{//didn't get all the http headers.
    			can_process=false;
    		}
    	}
    }
}
