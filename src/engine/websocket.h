/*
 * websocket.h
 *
 *  Created on: 25 במאי 2020
 *      Author: omer
 *      Copyright (C) 2020 omer levin <omerlle@gmail.com>
 */

#ifndef SRC_ENGINE_WEBSOCKET_H_
#define SRC_ENGINE_WEBSOCKET_H_
#include <QString>
#include <QMutex>
#include <QQueue>
#include <QThread>
#include <QTcpSocket>
#include <QTcpServer>

extern const QString COMPANY;
extern const QString PROGRAM;

class WebSocket : public QThread {
	struct ReplyData{
		int index;
		bool success;
		QString message;
		QByteArray payload;
		void reset(){
			index=-1;
			success=false;
			message="";
			payload.clear();
		}
	};
	struct WebSocketFrame{
		bool fin;
		int rsv;
		int opcode;
		bool bad_opcode;//use for case we in set of frames(not at the first one) and opcode !=0
		bool isMask;
		int headerSize;
		size_t size;
		size_t index;
		QByteArray mask;
		QByteArray data;
		void prepareForNextFrameInSet()
		{
			fin=false;
			rsv=-1;
			isMask=false;
			headerSize=0;
			size=0;
			index=0;
			mask.clear();
		}
		void reset()
		{
			prepareForNextFrameInSet();
			opcode=-1;
			data.clear();
		}
	};
	enum Commands
	{
		CMD_IDLE,
		CMD_START,
		CMD_END,
		CMD_IMPORT,
		CMD_GENERATE,
		CMD_EXPORT,
		CMD_GET_ACTIVITIES
	};
	Q_OBJECT
public:
	WebSocket(QWidget* gui);
	virtual ~WebSocket();
	void setReplyData(int index, bool success, const QString& message);
	void setReplyDataPayload(QByteArray & payload);
	void getXmlForImport(QByteArray & xml);
	void closeServer();
public slots:
		void reply();
		void sendStatus(QByteArray * status);
		void sendWarning(QByteArray * warning);
private slots:
	void newConnection();
	void disconnect();
	void handleRequest();
signals:
	void startRemoteControl(int index);
	void endRemoteControl(int index);
	void importXmlRules(int index);
	void getActivities(int index);
	void timetableGenerate(int index);
	void getResults(int index);
	void stopGenerate();
private:
	QString getCurrentOpcodeString();
	void sendCommand(Commands Command);
	void sendData(QByteArray * data);//delete data
	void encodeResponse(QByteArray &response,int opcode=1);
	QTcpServer *server;
	QTcpSocket *socket;
	QByteArray buffer;
	WebSocketFrame frameData;
	bool isWabSocketInit;
	ReplyData replyData;
	int correntIndex;
	Commands currentCommand;
	QQueue<Commands> pendingCommands;
	//use when the client disconnect in the middle of transaction.
	bool suppressResults;
	QByteArray xmlForImport;
};

#endif /* SRC_ENGINE_WEBSOCKET_H_ */
